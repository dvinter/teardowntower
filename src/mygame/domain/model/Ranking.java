/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.domain.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Diego
 */
public class Ranking implements Serializable {
    private Map<Level, List<RankingItem>> rankingList;

    public Ranking() {
        rankingList = new HashMap<Level, List<RankingItem>>();
    }
    
    public void addRanking(Level level, String playerName, float score) {
        RankingItem item = new RankingItem(playerName, score, level);
        
        List<RankingItem> rankingItems;
        if (rankingList.containsKey(level)) {
            rankingList.get(level).add(item);
        } else {
            rankingItems = new ArrayList<RankingItem>();
            rankingItems.add(item);
            rankingList.put(level, rankingItems);
        }
    }
    
    public List<RankingItem> getRanking (Level level) {
        return rankingList.get(level);
    }
    
    public List<RankingItem> getAllRankings () {
        List<RankingItem> items = new ArrayList<RankingItem>();
        for (List<RankingItem> l : rankingList.values()) {
            items.addAll(l);
        }
        return items;
    }
}
