/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.domain.model;

import java.io.Serializable;
import static mygame.utils.Utils.floatToPercent;
import static mygame.utils.Utils.TAB;

/**
 *
 * @author Diego
 */
public class RankingItem implements Serializable, Comparable<RankingItem> {
    private String name;
    private float score;
    private Level level;

    public RankingItem(String name, float score, Level level) {
        this.name = name;
        this.score = score;
        this.level = level;
    }

    public Level getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    public float getScore() {
        return score;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder()
            .append(level.getName()).append(TAB)
            .append(name.length() > 6 ? name.substring(0, 6) : name).append(TAB)
            .append(floatToPercent(score));
        return sb.toString();
    }

    public int compareTo(RankingItem anotherRankingItem) {
        if (this.score < anotherRankingItem.getScore()) {
            return 1;
        } else if (this.score > anotherRankingItem.getScore()) {
            return -1;
        }
        return 0;
    }
}
