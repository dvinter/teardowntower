/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.domain.model;

import java.io.Serializable;

/**
 *
 * @author Diego
 */
public enum Level implements Serializable {
    EASY("EASY  "), MEDIUM("MEDIUM"), HARD("HARD  ");
    
    private String name;

    private Level(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
