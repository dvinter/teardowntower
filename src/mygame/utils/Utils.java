/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.utils;

import java.awt.Image;
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author Diego
 */
public class Utils {
    private static final NumberFormat FORMATTER;
    public static final String TAB = "    ";
    
    static {
        FORMATTER = NumberFormat.getPercentInstance(new Locale("pt", "BR"));
        FORMATTER.setMaximumFractionDigits(2);
        FORMATTER.setMinimumFractionDigits(2);
        FORMATTER.setMaximumIntegerDigits(3);
        FORMATTER.setMinimumIntegerDigits(1);
        
    }
    
    public static boolean notNull (Object object) {
        return object != null;
    }
    
    public static boolean isNull (Object object) {
        return object ==  null;
    }
    
    public static String floatToPercent(float f) {
        return FORMATTER.format(f);
    }
    
    public static String getUserHome() {
        return System.getProperty("user.home");
    }
    
    public static Image getImage (String image) {
        return Toolkit.getDefaultToolkit().createImage(Utils.class.getClassLoader().getResource("Interface/" + image));
    }
}
