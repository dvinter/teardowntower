/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.state.AbstractAppState;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Menu;
import de.lessvoid.nifty.controls.MenuItemActivatedEvent;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import de.lessvoid.nifty.tools.SizeValue;
import java.awt.TrayIcon;
import javax.swing.JOptionPane;
import mygame.domain.model.Level;
import mygame.view.domain.model.MenuOption;
import static mygame.view.domain.model.MenuOption.ABOUT;
import static mygame.view.domain.model.MenuOption.AUDIO;
import static mygame.view.domain.model.MenuOption.DIFFICULTY;
import static mygame.view.domain.model.MenuOption.GOOGLE_CODE;
import static mygame.view.domain.model.MenuOption.HELP;
import static mygame.view.domain.model.MenuOption.NEW_GAME;
import static mygame.view.domain.model.MenuOption.RANKING;
import static mygame.view.domain.model.MenuOption.REFERENCES;
import org.bushe.swing.event.EventTopicSubscriber;

/**
 *
 * @author Diego
 */
public class GameScreenController extends AbstractAppState implements ScreenController {
    private NiftyImage sight;
    private Nifty nifty;
    private Screen screen;
    private Main view;
    private Element popupMenu;
    
    public GameScreenController(Main view) {
        this.view = view;
    }
    
    @Override
    public void bind(Nifty nifty, Screen screen) {
        this.nifty = nifty;
        this.screen = screen;
        this.sight = nifty.getRenderEngine().createImage(screen, "Interface/sight.png", false);
    }

    @Override
    public void onStartScreen() {
        startSight();
    }

    @Override
    public void onEndScreen() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
    }
    
    private void startSight () {
        Element sightElement = nifty.getCurrentScreen().findElementByName("sight");
        sightElement.setVisible(true);
    }
    
    public void showMenu () {
        createPopupMenu();
        view.disableControls();
        nifty.showPopup(nifty.getCurrentScreen(), popupMenu.getId(), null);
    }
    
    public void closeMenu (boolean startGame) {
        nifty.closePopup(popupMenu.getId());
        if (startGame) {
            view.startGame();
        }
    }
    
    private void createPopupMenu () {
        String audio = "Áudio " + (view.isAudioEnable() ? "HABILITADO" : "DESABILITADO");
        String level = "Dificuldade " + view.getLevel().getName();
        
        popupMenu =  nifty.createPopup("popupMenu");
        Menu menu = popupMenu.findNiftyControl("#menu", Menu.class);
        menu.setWidth(new SizeValue("300px"));
        menu.addMenuItem("Novo Jogo", MenuOption.NEW_GAME);
        menu.addMenuItemSeparator();
        menu.addMenuItem(audio, MenuOption.AUDIO);
        menu.addMenuItem(level, MenuOption.DIFFICULTY);
        menu.addMenuItemSeparator();
        menu.addMenuItem("Ranking", MenuOption.RANKING);
        menu.addMenuItem("Como Jogar?", MenuOption.HELP);
        menu.addMenuItem("Sobre...", MenuOption.ABOUT);
        menu.addMenuItem("Google Code", MenuOption.GOOGLE_CODE);
        menu.addMenuItem("Referências", MenuOption.REFERENCES);
        nifty.subscribe(nifty.getCurrentScreen(), menu.getId(), MenuItemActivatedEvent.class, new MenuItemActivatedEventSubscriber());
    }
    
    private class MenuItemActivatedEventSubscriber implements EventTopicSubscriber<MenuItemActivatedEvent> {
        public void onEvent(String string, MenuItemActivatedEvent event) {
            MenuOption option  = (MenuOption)event.getItem();
            switch (option) {
                case NEW_GAME:
                    String player = null;
                    do {
                        player = JOptionPane.showInputDialog("Digite o nome do jogador", view.getPlayerName());
                        if (player == null) {
                            return;
                        }
                    } while (player.trim().isEmpty());
                    view.setPlayerName(player);
                    closeMenu(true);
                    break;
                case RANKING:
                    view.showInfo(MenuOption.RANKING);
                    closeMenu(false);
                    break;
                case HELP:
                    view.showInfo(MenuOption.HELP);
                    closeMenu(false);
                    break;
                case ABOUT:
                    view.showInfo(MenuOption.ABOUT);
                    closeMenu(false);
                    break;
                case GOOGLE_CODE:
                    view.showInfo(MenuOption.GOOGLE_CODE);
                    closeMenu(false);
                    break;
                case REFERENCES:
                    view.showInfo(MenuOption.REFERENCES);
                    closeMenu(false);
                    break;
                case AUDIO:
                    closeMenu(false);
                    showSoundOptionMenu();
                    break;
                case DIFFICULTY:
                    closeMenu(false);
                    showLevelOptionMenu();
                    break;
                default:
                    break;
            }
        }
    }
        
    private void showSoundOptionMenu () {
        popupMenu =  nifty.createPopup("popupMenu");
        Menu menu = popupMenu.findNiftyControl("#menu", Menu.class);
        menu.setWidth(new SizeValue("300px"));
        menu.addMenuItem("Habilitar áudio", true);
        menu.addMenuItem("Desabilitar áudio", false);
        nifty.subscribe(nifty.getCurrentScreen(), menu.getId(), MenuItemActivatedEvent.class, new AudioItemActivatedEventSubscriber());
        nifty.showPopup(nifty.getCurrentScreen(), popupMenu.getId(), null);
    }
    
    private class AudioItemActivatedEventSubscriber implements EventTopicSubscriber<MenuItemActivatedEvent> {
        public void onEvent(String string, MenuItemActivatedEvent event) {
            view.setAudioEnable((Boolean)event.getItem());
            nifty.closePopup(popupMenu.getId());
            showMenu();
        }
    }
    
    private void showLevelOptionMenu () {
        popupMenu =  nifty.createPopup("popupMenu");
        Menu menu = popupMenu.findNiftyControl("#menu", Menu.class);
        menu.setWidth(new SizeValue("300px"));
        menu.addMenuItem(Level.EASY.getName(), Level.EASY);
        menu.addMenuItem(Level.MEDIUM.getName(), Level.MEDIUM);
        menu.addMenuItem(Level.HARD.getName(), Level.HARD);
        nifty.subscribe(nifty.getCurrentScreen(), menu.getId(), MenuItemActivatedEvent.class, new LevelItemActivatedEventSubscriber());
        nifty.showPopup(nifty.getCurrentScreen(), popupMenu.getId(), null);
    }
    
    private class LevelItemActivatedEventSubscriber implements EventTopicSubscriber<MenuItemActivatedEvent> {
        public void onEvent(String string, MenuItemActivatedEvent event) {
            view.setLevel((Level)event.getItem());
            nifty.closePopup(popupMenu.getId());
            showMenu();
        }
    }
}
