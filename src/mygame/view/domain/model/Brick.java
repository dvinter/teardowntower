/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.view.domain.model;

import com.jme3.material.Material;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;

/**
 *
 * @author Diego
 */
public class Brick extends Geometry {
    private static final Box BRICK_BOX;
    public static final String BRICK_NAME = "brick";
    public static final float BRICK_X_SIZE = 0.75f;
    public static final float BRICK_Y_SIZE = 0.25f;
    public static final float BRICK_Z_SIZE = 0.25f;
    
    static {
        BRICK_BOX = new  Box(BRICK_X_SIZE, BRICK_Y_SIZE, BRICK_Z_SIZE);
    }

    public Brick(Material material) {
        super(BRICK_NAME, BRICK_BOX);
        init(material);
    }
    
    private void init(Material material) {
        setMaterial(material);
        setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
    }
}
