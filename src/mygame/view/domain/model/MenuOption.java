/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.view.domain.model;

/**
 *
 * @author Diego
 */
public enum MenuOption {
    NEW_GAME, RANKING, HELP, ABOUT, GOOGLE_CODE, REFERENCES, AUDIO, DIFFICULTY
}
