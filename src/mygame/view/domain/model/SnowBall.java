/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.view.domain.model;

import com.jme3.material.Material;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Sphere;

/**
 *
 * @author Diego
 */
public class SnowBall extends Geometry {
    private static final Sphere SPHERE;
    public static final String BULLET_NAME = "bullet";
    
    static {
        SPHERE = new Sphere(32, 32, 0.4f, true, false);
        SPHERE.setTextureMode(Sphere.TextureMode.Projected);
    }

    public SnowBall(Material material) {
        super(BULLET_NAME, SPHERE);
        init(material);
    }

    private void init(Material material) {
        setMaterial(material);
        setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
    }
}
