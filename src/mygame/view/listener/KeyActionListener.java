/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.view.listener;

import com.jme3.input.controls.ActionListener;
import mygame.controller.Controller;

/**
 *
 * @author Diego
 */
public class KeyActionListener implements ActionListener {
    public static final String RESTART_ACTION_NAME = "restart";
    public static final String MENU_ACTION_NAME = "menu";
    
    private Controller controller;

    public KeyActionListener(Controller controller) {
        this.controller = controller;
    }
    
    public void onAction(String name, boolean isPressed, float tpf) {
        if (name.equals(RESTART_ACTION_NAME)) {
            controller.startGame();
        } else if (name.equals(MENU_ACTION_NAME)) {
            controller.showMenu();
        }
    }
    
}
