/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.view.listener;

import com.jme3.input.controls.ActionListener;
import com.jme3.material.Material;
import mygame.controller.Controller;
import mygame.view.domain.model.SnowBall;

/**
 *
 * @author Diego
 */
public class ShootActionListener implements ActionListener {
    public static final String SHOOT_ACTION_NAME = "shoot";
    private Controller controller;
    private Material bulletMaterial;

    public ShootActionListener(Controller controller, Material bulletMaterial) {
        this.controller =  controller;
        this.bulletMaterial = bulletMaterial;
    }
    
    public void onAction(String name, boolean isPressed, float tpf) {
        if (name.equals(SHOOT_ACTION_NAME) && isPressed && !controller.isEmptyAmmunition()) {
            controller.shoot();
            SnowBall bullet = new SnowBall(bulletMaterial);
            controller.attachBullet(bullet);
        }
    }
}
