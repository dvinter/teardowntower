/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.controller;

import com.jme3.audio.AudioNode;
import com.jme3.math.Vector3f;
import java.io.File;
import java.util.Collections;
import java.util.List;
import mygame.Main;
import mygame.domain.model.Level;
import mygame.domain.model.Ranking;
import mygame.domain.model.RankingItem;
import mygame.utils.SerializeUtils;
import mygame.utils.Utils;
import mygame.view.domain.model.SnowBall;

/**
 *
 * @author Diego
 */
public class Controller {
    private static final String RANKING_FILE_NAME = "ranking.tdt";
    
    private int ammunition;
    private Main view;
    private boolean audioEnable;
    private Level level;
    private String playerName;
    private Ranking ranking;
    
    private AudioNode shootSound;
    private AudioNode ambientSound;
    private AudioNode finishSound;
    private boolean endGame;
    
    
    public Controller (Main view) {
        this.view = view;
        this.ranking = new Ranking();
        this.level = Level.EASY;
        this.endGame = true;
        this.playerName = "";
        
        initAudios();
        enableAudio();
        
        File rankingFile = new File(System.getProperty("user.home"), RANKING_FILE_NAME);
        if (rankingFile.exists()) {
            ranking  = (Ranking)SerializeUtils.deserializeFrom(rankingFile);
            if (Utils.isNull(ranking)) {
                ranking = new Ranking();
            }
        }
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }
    
    public void start () {
        this.endGame = false;
        switch (level) {
            case EASY:
                ammunition = 12;
                break;
            case MEDIUM:
                ammunition = 10;
                break;
            case HARD:
                ammunition = 8;
                break;
        }        
    }

    public boolean isEndGame() {
        return endGame;
    }
    
    public boolean isEmptyAmmunition () {
        return ammunition == 0;
    }

    public int getAmmunition() {
        return ammunition;
    }
    
    public void shoot () {
        ammunition--;
        playShootSound();
    }
    
    public void attachBullet(SnowBall bullet) {
        view.attachBullet(bullet);
    }

    private void initAudios () {
        ambientSound = new AudioNode(view.getAssetManager(), "Sounds/wind.wav", false);
        ambientSound.setLooping(true);
        ambientSound.setPositional(false);
        ambientSound.setLocalTranslation(Vector3f.ZERO.clone());

        shootSound = new AudioNode(view.getAssetManager(), "Sounds/shoot.wav", false);
        shootSound.setVolume(3);
        shootSound.setPositional(false);
//        
//        finishSound = new AudioNode(view.getAssetManager(), "", false);
//        finishSound.setVolume(3);
//        finishSound.setPositional(false);
    }
    
    public void playAmbientSound () {
        ambientSound.play();
    }
    
    public void stopAmbientSound () {
        ambientSound.stop();
    }
    
    public void playShootSound () {
        if (audioEnable) {
            shootSound.playInstance();
        }
    }

    public Level getLevel() {
        return level;
    }
    
    public void playFinishSound () {
        if (audioEnable) {
            finishSound.play();
        }
    }
    
    public void disableAudio () {
        audioEnable = false;
        stopAmbientSound();
    }
    
    public void enableAudio () {
        audioEnable = true;
        playAmbientSound();
    }

    public boolean isAudioEnable() {
        return audioEnable;
    }
    
    public void showMenu () {
        view.showMenu();
    }
    
    public String getRanking () {
        StringBuilder sb = new StringBuilder();
        List<RankingItem> items = ranking.getRanking(level);
        if (Utils.isNull(items)) {
            return "";
        } else {
            Collections.sort(items);
            int size = items.size() > 10 ? 10 : items.size();
            for (int i = 0; i < size; i++) {
                sb.append(items.get(i).toString()).append(System.getProperty("line.separator"));
            }
            return sb.toString();
        }
    }
    
    public void endGame (float score) {
        endGame = true;
        ranking.addRanking(level, playerName, score);
        SerializeUtils.serializeTo(new File(Utils.getUserHome(), RANKING_FILE_NAME), ranking);
    }
    
    public void startGame () {
        view.startGame();
    }
}
